<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <meta name="description" content="calculator"/>
                <link href="css/index.css" rel="stylesheet"/>
                <link href="plants/plants.css" rel="icon"/>
                <title>Plants</title>
            </head>
        </html>
    </xsl:template>
</xsl:stylesheet>