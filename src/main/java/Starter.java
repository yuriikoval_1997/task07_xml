import com.jcabi.xml.XMLDocument;
import model.Plant;
import parsers.dom.DOMCreator;
import parsers.sax.SchemaValidator;
import parsers.dom.DOMPlantReader;
import parsers.sax.SAXParserExecutor;
import parsers.stax.StAXParserExecutor;


import java.io.File;
import java.text.ParseException;
import java.util.List;

public class Starter {
    public static void main(String[] args) throws ParseException {
        File xml = new File("D:\\EPAM_studying\\task07_xml\\src\\main\\resources\\plants_xml\\plants.xml");
        File xsd = new File("D:\\EPAM_studying\\task07_xml\\src\\main\\resources\\plants_xml\\plants.xsd");
        String validationResult = SchemaValidator.validate(xml, xsd);

        //Prints the result using DOM
        System.out.println("==== Read by DOM ====");
        List<Plant> plants = DOMPlantReader.readDoc(DOMCreator.getInstance().createDocument(xml));
        plants.forEach(e -> System.out.println(e + "\n\n"));

        //Prints the result using SAX
        System.out.println("==== Read by SAX ====");
        List<Plant> plantList = SAXParserExecutor.parsePlants(xml, xsd);
        plantList.forEach(e -> System.out.println(e + "\n\n"));

        //TODO - it's not usable yet.
//        //Prints the result using StAX
//        System.out.println("==== Read by StAX ====");
//        List<Plant> plantsStAXList = StAXParserExecutor.parsePlants(xml, xsd);
//        plantsStAXList.forEach(e -> System.out.println(e + "\n\n"));
    }

    /**
     * This method prints into the console .xml
     *
     * Required dependency
     * <dependency>
     *             <groupId>com.jcabi</groupId>
     *             <artifactId>jcabi-xml</artifactId>
     *             <version>0.14</version>
     *         </dependency>
     * @param xml
     */
    private static void printXMLTreeUsingJCABI(File xml){
        String xmlTree = new XMLDocument(DOMCreator.getInstance().createDocument(xml)).toString();
        System.out.println(xmlTree);
    }
}
