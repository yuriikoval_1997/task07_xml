package extension_checker;


import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class ExtensionChecker {
    public static boolean isXML(File xml){
        boolean isFile = xml.isFile();
        boolean isXML = xml.getName().endsWith(".xml");
        return isFile && isXML;
    }

    public static boolean isXSD(File xsd){
        boolean isFile = xsd.isFile();
        boolean isXSD = FilenameUtils.getExtension(xsd.getName()).equals("xsd");
        return isFile && isXSD;
    }
}
