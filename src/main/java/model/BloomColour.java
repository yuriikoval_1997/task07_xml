package model;

public enum  BloomColour {
    RED,
    ORANGE,
    YELLOW,
    BLUE,
    VIOLET,
    WHITE,
    PURPLE,
    PINK
}
