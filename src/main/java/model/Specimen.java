package model;

import java.util.Objects;

public class Specimen {
    private double latitude;
    private double longitude;
    private String plantedBy;
    private String comments;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPlantedBy() {
        return plantedBy;
    }

    public void setPlantedBy(String plantedBy) {
        this.plantedBy = plantedBy;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Specimen)) return false;
        Specimen specimen = (Specimen) o;
        return Double.compare(specimen.latitude, latitude) == 0 &&
                Double.compare(specimen.longitude, longitude) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public String toString() {
        return "Specimen{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", plantedBy='" + plantedBy + '\'' +
                ", comments='" + comments + '\'' +
                '}';
    }
}
