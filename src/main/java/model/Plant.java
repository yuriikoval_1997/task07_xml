package model;

import java.util.Date;
import java.util.HashSet;

public class Plant {
    private String genus;
    private String species;
    private String cultivar;
    private String commonName;
    private int height;
    private Date beginBloomingDate;
    private Date endDloomingDate;
    private boolean edible;
    private SunTolerance sunTolerance;
    private BloomColour bloomColour;
    private HashSet<Specimen> specimens;

    public String getGenus() {
        return genus;
    }

    public void setGenus(String genus) {
        this.genus = genus;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getCultivar() {
        return cultivar;
    }

    public void setCultivar(String cultivar) {
        this.cultivar = cultivar;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Date getBeginBloomingDate() {
        return beginBloomingDate;
    }

    public void setBeginBloomingDate(Date beginBloomingDate) {
        this.beginBloomingDate = beginBloomingDate;
    }

    public Date getEndDloomingDate() {
        return endDloomingDate;
    }

    public void setEndDloomingDate(Date endDloomingDate) {
        this.endDloomingDate = endDloomingDate;
    }

    public boolean isEdible() {
        return edible;
    }

    public void setEdible(boolean edible) {
        this.edible = edible;
    }

    public SunTolerance getSunTolerance() {
        return sunTolerance;
    }

    public void setSunTolerance(SunTolerance sunTolerance) {
        this.sunTolerance = sunTolerance;
    }

    public BloomColour getBloomColour() {
        return bloomColour;
    }

    public void setBloomColour(BloomColour bloomColour) {
        this.bloomColour = bloomColour;
    }

    public HashSet<Specimen> getSpecimens() {
        return specimens;
    }

    public void setSpecimens(HashSet<Specimen> specimens) {
        this.specimens = specimens;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        specimens.forEach(e -> stringBuilder.append("\t").append(e).append("\n"));
        return "Plant{" +
                "genus='" + genus + "\'\n" +
                ", species='" + species + "\'\n" +
                ", cultivar='" + cultivar + "\'\n" +
                ", commonName='" + commonName + "\'\n" +
                ", height=" + height + "\n" +
                ", beginBloomingDate=" + beginBloomingDate + "\n" +
                ", endDloomingDate=" + endDloomingDate + "\n" +
                ", edible=" + edible + "\n" +
                ", sunTolerance=" + sunTolerance + "\n" +
                ", bloomColour=" + bloomColour + "\n" +
                ", specimens={" + stringBuilder.toString() + "\n" +
                '}';
    }
}
