package model;

public enum  SunTolerance {
    FULL_SUN,
    PART_SUN,
    PART_SHADE,
    FULL_SHADE
}
