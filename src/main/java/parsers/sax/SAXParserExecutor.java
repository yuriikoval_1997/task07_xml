package parsers.sax;

import model.Plant;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class SAXParserExecutor {
    private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

    public static List<Plant> parsePlants(File xml, File xsd){
        List<Plant> plants = null;
        try {
            // If you don't set schema .xsd, you won't be able to access xsd features (for instance, to get default value)
            saxParserFactory.setSchema(SchemaValidator.getW3CSchema(xsd));
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SAXPlantHandler saxPlantHandler = new SAXPlantHandler();
            saxParser.parse(xml, saxPlantHandler);
            plants = saxPlantHandler.getPlantList();
        } catch (SAXException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
        return plants;
    }
}
