package parsers.sax;

import model.Plant;
import model.Specimen;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import parsers.dom.DOMPlantReader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class SAXPlantHandler extends DefaultHandler {
    private List<Plant> plantList;
    private Plant plant;
    private Specimen specimen;
    private HashSet<Specimen> specimens;
    private boolean bGenus;
    private boolean bSpecies;
    private boolean bCultivar;
    private boolean bCommon_name;
    private boolean bHeight;
    private boolean bBeginBloomDate;
    private boolean bEndBloomDate;
    private boolean bEdible;
    private boolean bSunTolerance;
    private boolean bBloomColour;
    private boolean bLatitude;
    private boolean bLongitude;
    private boolean bPlantedBy;
    private boolean bComments;

    public List<Plant> getPlantList(){
        return plantList;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        // qName - a name with the prefix.
        switch (qName.trim()){
            case "plants":
                plantList = new ArrayList<>();
                break;
            case "plant":
                plant = new Plant();
                break;
            case "genus":
                bGenus = true;
                break;
            case "species":
                bSpecies = true;
                break;
            case "cultivar":
                bCultivar = true;
                break;
            case "common_name":
                bCommon_name = true;
                break;
            case "height":
                bHeight = true;
                break;
            case "begin_bloom_date":
                bBeginBloomDate = true;
                break;
            case "end_bloom_date":
                bEndBloomDate = true;
                break;
            case "edible":
                bEdible = true;
                break;
            case "sun_tolerance":
                bSunTolerance = true;
                break;
            case "bloom_colour":
                bBloomColour = true;
                break;
            // Field of a specimens
            case "specimens":
                specimens = new HashSet<>();
                break;
            case "specimen":
                specimen = new Specimen();
                break;
            case "latitude":
                bLatitude = true;
                break;
            case "longitude":
                bLongitude = true;
                break;
            case "planted_by":
                bPlantedBy = true;
                break;
            case "comments":
                bComments = true;
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equals("plant")){
            plant.setSpecimens(specimens);
            plantList.add(plant);
        } else if (qName.equals("specimens")){
            specimens.add(specimen);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String tagValue = new String(ch, start, length).trim();
        try {
            if (bGenus){
                plant.setGenus(tagValue);
                bGenus = false;
            } else if (bSpecies){
                plant.setSpecies(tagValue);
                bSpecies = false;
            } else if (bCultivar){
                plant.setCultivar(tagValue);
                bCultivar = false;
            } else if (bCommon_name){
                plant.setCommonName(tagValue);
                bCommon_name = false;
            } else if (bHeight){
                plant.setHeight(Integer.parseInt(tagValue));
                bHeight = false;
            } else if (bBeginBloomDate){
                final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                plant.setBeginBloomingDate(sdf.parse(tagValue));
                bBeginBloomDate = false;
            } else if (bEndBloomDate){
                final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                plant.setEndDloomingDate(sdf.parse(tagValue));
                bEndBloomDate = false;
            } else if (bEdible){
                plant.setEdible(Boolean.parseBoolean(tagValue));
                bEdible = false;
            } else if (bSunTolerance){
                plant.setSunTolerance(DOMPlantReader.getSunTolerance(tagValue));
                bSunTolerance = false;
            } else if (bBloomColour){
                plant.setBloomColour(DOMPlantReader.getBloomColour(tagValue));
                bBloomColour = false;
            }
            // Field of specimens
            else if (bLatitude){
                specimen.setLatitude(Double.parseDouble(tagValue));
                bLatitude = false;
            } else if (bLongitude){
                specimen.setLongitude(Double.parseDouble(tagValue));
                bLongitude = false;
            } else if (bPlantedBy){
                specimen.setPlantedBy(tagValue);
                bPlantedBy = false;
            } else if (bComments){
                specimen.setComments(tagValue);
                bComments = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
