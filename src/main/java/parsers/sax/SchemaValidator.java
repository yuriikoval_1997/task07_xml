package parsers.sax;

import extension_checker.ExtensionChecker;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class SchemaValidator {
    public static String validate(File xml, File xsd){
        if (!ExtensionChecker.isXML(xml) || !ExtensionChecker.isXSD(xsd)){
            throw new IllegalArgumentException();
        }
        Schema schema = getW3CSchema(xsd);
        Validator validator = schema.newValidator();
        Source xmlSource = new StreamSource(xml);
        try {
            validator.validate(xmlSource);
            return "Valid";
        } catch (SAXException e) {
            return xmlSource.getSystemId() + " is NOT valid reason:" + e;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Schema getW3CSchema(File xsd){
        Schema schema = null;
        try {
            String schemaLanguage = XMLConstants.W3C_XML_SCHEMA_NS_URI;
            SchemaFactory schemaFactory = SchemaFactory.newInstance(schemaLanguage);
            schema = schemaFactory.newSchema(xsd);
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return schema;
    }
}
