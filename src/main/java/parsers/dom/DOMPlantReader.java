package parsers.dom;

import model.BloomColour;
import model.Plant;
import model.Specimen;
import model.SunTolerance;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class DOMPlantReader {
    private static final int firstChildIndex = 0;

    public static List<Plant> readDoc(Document doc) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<Plant> plants = new ArrayList<>();
        NodeList nodeList = doc.getElementsByTagName("plant");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Plant plant = new Plant();
            Node node = nodeList.item(i);
            if (node instanceof Element){
                Element element = (Element) node;
                plant.setGenus(element.getElementsByTagName("genus")
                        .item(firstChildIndex).getTextContent().trim());
                plant.setSpecies(element.getElementsByTagName("species")
                        .item(firstChildIndex).getTextContent().trim());
                // FIXME: 31.10.2018 - throws NullPointerException if an optional tag is absent
                plant.setCultivar(element.getElementsByTagName("cultivar")
                        .item(firstChildIndex).getTextContent().trim());
                // FIXME: 31.10.2018 - throws NullPointerException if an optional tag is absent
                plant.setCommonName(element.getElementsByTagName("common_name")
                        .item(firstChildIndex).getTextContent().trim());
                // FIXME: 31.10.2018 - throws NullPointerException if an optional tag is absent
                plant.setHeight(Integer.parseInt(element.getElementsByTagName("height")
                        .item(firstChildIndex).getTextContent().trim()));
                plant.setBeginBloomingDate(sdf.parse(element.getElementsByTagName("begin_bloom_date")
                        .item(firstChildIndex).getTextContent().trim()));
                plant.setEndDloomingDate(sdf.parse(element.getElementsByTagName("end_bloom_date")
                        .item(firstChildIndex).getTextContent().trim()));
                plant.setEdible(Boolean.parseBoolean(element.getElementsByTagName("edible")
                        .item(firstChildIndex).getTextContent().trim()));
                plant.setSunTolerance(getSunTolerance(element.getElementsByTagName("sun_tolerance")
                        .item(firstChildIndex).getTextContent().trim()));
                plant.setBloomColour(getBloomColour(element.getElementsByTagName("bloom_colour")
                        .item(firstChildIndex).getTextContent().trim()));
                plant.setSpecimens(readSpecimens(element));
                plants.add(plant);
            }
        }
        return plants;
    }

    private static HashSet<Specimen> readSpecimens(Element element){
        HashSet<Specimen> specimens = new HashSet<>();
        // FIXME: 31.10.2018 - throws NullPointerException if an optional tag is absent
        Element specimenEl = (Element) element.getElementsByTagName("specimens").item(firstChildIndex);
        NodeList nodeList = specimenEl.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element){
                Element nextEl = (Element) node;
                Specimen specimen = new Specimen();
                specimen.setLatitude(Double.parseDouble(nextEl.getElementsByTagName("latitude")
                        .item(firstChildIndex).getTextContent().trim()));
                specimen.setLongitude(Double.parseDouble(nextEl.getElementsByTagName("longitude")
                        .item(firstChildIndex).getTextContent().trim()));
                // FIXME: 31.10.2018 - throws NullPointerException if an optional tag is absent
                specimen.setPlantedBy(nextEl.getElementsByTagName("planted_by")
                        .item(firstChildIndex).getTextContent().trim());
                // FIXME: 31.10.2018 - throws NullPointerException if an optional tag is absent
                specimen.setComments(nextEl.getElementsByTagName("comments")
                        .item(firstChildIndex).getTextContent().trim());
                specimens.add(specimen);
            }
        }
        return specimens;
    }

    public static BloomColour getBloomColour(String colour){
        switch (colour){
            case "red":
                return BloomColour.RED;
            case "orange":
                return BloomColour.ORANGE;
            case "yellow":
                return BloomColour.YELLOW;
            case "blue":
                return BloomColour.BLUE;
            case "violet":
                return BloomColour.VIOLET;
            case "white":
                return BloomColour.WHITE;
            case "purple":
                return BloomColour.PURPLE;
            case "pink":
                return BloomColour.PINK;
            default:
                throw new IllegalArgumentException("There is no such a colour: " + colour);
        }
    }

    public static SunTolerance getSunTolerance(String type){
        switch (type){
            case "full_sun":
                return SunTolerance.FULL_SUN;
            case "part_sun":
                return SunTolerance.PART_SUN;
            case "part_shade":
                return SunTolerance.PART_SHADE;
            case "full_shade":
                return SunTolerance.FULL_SHADE;
            default:
                throw new IllegalArgumentException("There is no such a type: " + type);
        }
    }
}
