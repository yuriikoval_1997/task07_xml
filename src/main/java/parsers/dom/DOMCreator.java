package parsers.dom;

import extension_checker.ExtensionChecker;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DOMCreator {
    private static DOMCreator domCreator;
    private DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

    private DOMCreator(){}

    public static synchronized DOMCreator getInstance(){
        if (domCreator == null){
            domCreator = new DOMCreator();
        }
        return domCreator;
    }

    public Document createDocument(File xml){
        if (! ExtensionChecker.isXML(xml)){
            throw new IllegalArgumentException();
        }
        DocumentBuilder documentBuilder = getDocumentBuilder();
        Document document = null;
        try {
            document = documentBuilder.parse(xml);
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    private DocumentBuilder getDocumentBuilder(){
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder =  documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return documentBuilder;
    }
}
