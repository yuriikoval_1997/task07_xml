package parsers.stax;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import java.io.InputStream;

public class StAXEventProcessor implements AutoCloseable {
    private static final XMLInputFactory FACTORY = XMLInputFactory.newInstance();
    private final XMLEventReader xmlEventReader;

    public StAXEventProcessor(InputStream inputStream) throws XMLStreamException {
        xmlEventReader = FACTORY.createXMLEventReader(inputStream);
    }

    public XMLEventReader getXmlEventReader(){
        return xmlEventReader;
    }

    @Override
    public void close() {
        if (xmlEventReader != null){
            try {
                xmlEventReader.close();
            } catch (XMLStreamException e){
                e.printStackTrace();
            }
        }
    }
}
