package parsers.stax;

import model.Plant;
import model.Specimen;
import parsers.dom.DOMPlantReader;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class StAXParserExecutor {
    public static List<Plant> parsePlants(File xml, File xsd){
        List<Plant> plants = new ArrayList<>();
        Plant plant = null;
        Specimen specimen = null;
        HashSet<Specimen> specimens = null;

        try(StAXEventProcessor processor = new StAXEventProcessor(new FileInputStream(xml))){
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            XMLEventReader xmlEventReader = processor.getXmlEventReader();
            while (xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String tagName = startElement.getName().getLocalPart();
                    switch (tagName.trim()){
                        case "plant":
                            plant = new Plant();
                            break;
                        case "genus":
                            plant.setGenus(startElement.asCharacters().getData());
                            break;
                        case "species":
                            plant.setSpecies(startElement.asCharacters().getData());
                            break;
                        case "cultivar":
                            plant.setSpecies(startElement.asCharacters().getData());
                            break;
                        case "common_name":
                            plant.setCommonName(startElement.asCharacters().getData());
                            break;
                        case "height":
                            plant.setHeight(Integer.parseInt(startElement.asCharacters().getData()));
                            break;
                        case "begin_bloom_date":
                            plant.setBeginBloomingDate(sdf.parse(startElement.asCharacters().getData()));
                            break;
                        case "end_bloom_date":
                            plant.setEndDloomingDate(sdf.parse(startElement.asCharacters().getData()));
                            break;
                        case "edible":
                            plant.setEdible(Boolean.parseBoolean(startElement.asCharacters().getData()));
                            break;
                        case "sun_tolerance":
                            plant.setSunTolerance(DOMPlantReader.getSunTolerance(startElement.asCharacters().getData()));
                            break;
                        case "bloom_colour":
                            plant.setBloomColour(DOMPlantReader.getBloomColour(startElement.asCharacters().getData()));
                            break;
                        // Field of a specimens
                        case "specimens":
                            specimens = new HashSet<>();
                            break;
                        case "specimen":
                            specimen = new Specimen();
                            break;
                        case "latitude":
                            specimen.setLatitude(Double.parseDouble(startElement.asCharacters().getData()));
                            break;
                        case "longitude":
                            specimen.setLongitude(Double.parseDouble(startElement.asCharacters().getData()));
                            break;
                        case "planted_by":
                            specimen.setPlantedBy(startElement.asCharacters().getData());
                            break;
                        case "comments":
                            specimen.setComments(startElement.asCharacters().getData());
                            break;
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException | ParseException e) {
            e.printStackTrace();
        }
        return plants;
    }
}
