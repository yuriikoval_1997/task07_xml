package comparator;

import model.Plant;

import java.util.Comparator;

public class PlantComparator implements Comparator<Plant> {

    @Override
    public int compare(Plant plant, Plant t1) {
        String firstPlant = plant.getGenus() + plant.getSpecies();
        String secondPlant = plant.getGenus() + plant.getSpecies();
        return firstPlant.compareTo(secondPlant);
    }
}
